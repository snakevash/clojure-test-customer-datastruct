(ns clojure-test-customer-datastruct.mydeftype
  (:import (clojure.lang IDeref)))

;; 被设计用来定义最底层的类型
;; 新的数据结构 引用类型
;; 一个是应用级别 一个是框架级别
;; 特性：可修改的字段

;; 定义的类型
;; volatile和synchroinzed
;; ^:volatile-mutable ^:unsynchronized-mutable
(deftype MyType [^:volatile-mutable fld])

;; 不可修改字段始终是public 可修改字段始终是private
;; IDeref接口  可以解引用

(deftype SchrodingerCat [^:unsynchronized-mutable state]
  IDeref
  (deref [sc]
    (locking sc
      ;; sc只是锁的一个载体对象
      (or state
          (set! state (if (zero? (rand-int 2))
                        :dead
                        :alive))))))

(defn schrodinger-cat
  "创建一条猫 repl会杀死它"
  []
  (SchrodingerCat. nil))

;; 应用的可修改性
;; agents atoms refs
;; futures promises delays

