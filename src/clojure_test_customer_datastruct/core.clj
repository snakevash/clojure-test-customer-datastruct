(ns clojure-test-customer-datastruct.core)

(defrecord Point [x y])
(deftype PointType [x y])

;; 字段以public和final来修饰
;; 采用驼峰风格
;; 访问更新这些字段比操作clojure的map数据结构要快的多
;; java互操作访问字段
;; 每个字段都是java.lang.Object类型
;; 通过类型提示来定义基本类型但是不能通过类型提示来改变非基本类型


(defrecord NamedPoint [^String name ^long x ^long y])
;; 定义一个具体有long类型的，名字分别为x和y的两个字段
;; name虽然提示为string，但是还是Object类型

;; 记录类型支持在运行时添加字段
;; 能够知道类型有那些字段

(NamedPoint/getBasis)

;; 保持了定义时的元数据

(map meta (NamedPoint/getBasis))

;; defrecord 提供了互操作的一些默认行为
;; deftype 提供了底层优化的能力

;; 定义好的类型被定义在跟所在命名空间对应的一个java package包
;; 默认引入所定义的命令空间里面
;; 其他命令空间需要显示import

